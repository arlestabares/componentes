import 'package:flutter/material.dart';
import 'package:componentes/src/pages/alert_page.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

//import 'package:componentes/src/pages/alert_page.dart';
//import 'package:componentes/src/pages/avatar_page.dart';
//import 'package:componentes/src/pages/home_page_json.dart';
import 'package:componentes/src/routes/routes.dart';

//import 'package:componentes/src/pages/home_page.dart';
//import 'package:componentes/src/pages/home_temp.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Componentes APP',
      debugShowCheckedModeBanner: false,

      localizationsDelegates: [
        // ... delegado[s] de localización específicos de la app aquí
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'), // Inglés
        const Locale('es'), // Español
       
      ],
      // home: HomePageJson(),
      //Aqui estaran definidas las rutas de acceso para ir hacia otras
      //paginas dentro de la aplicacion, de esta manera se podra saber
      //cuales seran las rutas disponibles en la aplicacion,
      initialRoute: '/',
      //  routes: <String, WidgetBuilder> {
      //    '/'     : (BuildContext context )=> HomePageJson(),
      //    'alert' : (BuildContext context )=> AlertPage(),
      //   'avatar' : (BuildContext context) => AvatarPage(),

      //  },
      routes: getApplicationRoute(),

      //onGenerateRoute, este metodo lo que nos provee es
      // que si la ruta no se encuentra en las rutas definidas previamente en nuestro 
      //archivo de rutas,entonces disparamos el metodo MaterialPageRoute()
      //que nos llevara a una clase que le diremos puntualmente.
      onGenerateRoute: (RouteSettings setting) {
        return MaterialPageRoute(
            builder: (BuildContext context) => AlertPage());
      },
    );
  }
}
