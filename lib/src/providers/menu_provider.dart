import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

/*
 * La clase la hago privada con _MenuProvider, de esta manera solo esta exponiendo
 *la instancia creada  del _menuProvider creada al final de la clase y fuera de ella.
 *
 */
class _MenuProvider {
  List<dynamic> opciones = [];

  _MenuProvider() {
    // ya no necesito  llamar al constructor para cargar la data
    //cargarData();
  }

/*
 *Metodo  encargado de la lectura de la data  contenida en el json
 */
  Future<List<dynamic>> cargarData() async {
    //rootBundle tiene lo que necesito para leer la data.

    final resp = await rootBundle.loadString('data/menu_opts.json');
    Map dataMap = json.decode(resp);
    print(dataMap['rutas']);
    opciones = dataMap['rutas']; // con esto lleno la lista con los valores almacenados en el Json.

    return opciones;
  }
} //cierre de clase

//creo una instancia de Menuprovider(), que sera llamada
//en HomePage dentro del metodo  Widget _lista()
final menuProvider = new _MenuProvider();
