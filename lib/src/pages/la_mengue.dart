import 'package:componentes/src/pages/la_mengue2.dart';
import 'package:flutter/material.dart';

class LaMengue extends StatelessWidget {
  const LaMengue({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('La Mengue'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16).copyWith(top: 10),
        child: Center(
          child: Text('La Mengue'),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Text('La mengue'),
        onPressed: () => Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => LaMengue2())),
      ),
    );
  }
}
