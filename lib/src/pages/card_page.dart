import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CardPage'),
        centerTitle: true,
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo3(),
          SizedBox(height: 30.0),
          _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo4(),
          SizedBox(height: 30.0),
          _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo5(),
          SizedBox(height: 30.0),
          _cardTipo6(),
          // SizedBox(height: 30.0),
          // _cardTipo2(),
          // SizedBox(height: 30.0),
          // _cardTipo1(),
          // SizedBox(height: 30.0),
          // _cardTipo2(),
          // SizedBox(height: 30.0)
        ],
      ),
    );
  }

  Widget _cardTipo1() {
    return Card(
      elevation: 10.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.photo_album, color: Colors.blue),
            title: Text('Trabajo con Cards'),
            subtitle: Text(
                'Subtitulo de la tarjeta Cards para demostracion de curso flutter'),
          ),
          Row(
            //con la siguiente linea de codigo alineo al final del card los elementos
            //cancelar y ok
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              TextButton(child: Text('Cancelar'), onPressed: () {}),
              TextButton(child: Text('Ok'), onPressed: () {}),
            ],
          ),
        ],
      ),
    );
  }

  // Widget _cardTipo2() {
  //   final card = Container(
  //     child: Column(
  //       children: <Widget>[
  //         FadeInImage(
  //           image: NetworkImage(
  //               'https://iso.500px.com/wp-content/uploads/2014/07/big-one.jpg'),
  //           placeholder: AssetImage('assets/jar-loading.gif'),
  //           fadeInDuration: Duration(milliseconds: 200),
  //           height: 300.0,
  //           fit: BoxFit.cover,
  //         ),
  //         Container(
  //           padding: EdgeInsets.all(10.0),
  //           child: Text('Imagen descargada de Internet'),
  //         ),
  //       ],
  //     ),
  //   );
  //   return Container(
  //     decoration: BoxDecoration(
  //         borderRadius: BorderRadius.circular(30.0), color: Colors.white),
  //     child: ClipRRect(
  //       borderRadius: BorderRadius.circular(30.0),
  //       child: card,
  //     ),
  //   );
  // }

  Widget _cardTipo3() {
    final card = Container(
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: AssetImage('assets/montana-nevada.jpeg'),
            // image: NetworkImage(
            //  'https://iso.500px.com/wp-content/uploads/2014/07/big-one.jpg'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 300.0,
            fit: BoxFit.cover,
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Imagen descargada de Internet'),
          ),
        ],
      ),
    );
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0), color: Colors.white),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }

  Widget _cardTipo4() {
    final card = Container(
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: AssetImage('assets/campo.jpg'),
            // image: NetworkImage(
            //  'https://iso.500px.com/wp-content/uploads/2014/07/big-one.jpg'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 300.0,
            fit: BoxFit.cover,
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Imagen descargada de Internet'),
          ),
        ],
      ),
    );
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0), color: Colors.white),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }

  Widget _cardTipo5() {
    final card = Container(
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: AssetImage('assets/bosque.jpg'),
            // image: NetworkImage(
            //  'https://iso.500px.com/wp-content/uploads/2014/07/big-one.jpg'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 300.0,
            fit: BoxFit.cover,
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Imagen descargada de Internet'),
          ),
        ],
      ),
    );
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0), color: Colors.white),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }

  Widget _cardTipo6() {
    final card = Container(
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: AssetImage('assets/ilucion.jpg'),
            // image: NetworkImage(
            //  'https://iso.500px.com/wp-content/uploads/2014/07/big-one.jpg'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 300.0,
            fit: BoxFit.cover,
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Imagen descargada de Internet'),
          ),
        ],
      ),
    );
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0), color: Colors.white),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }
}
