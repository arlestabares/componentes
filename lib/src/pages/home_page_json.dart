import 'package:flutter/material.dart';
import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';

class HomePageJson extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Leer Json'),
      ),
      body: _lista(),
    );
  }

  Widget _lista() {
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],//el initialData  sera una lista vacia, que es el valor que 
      //se le enviara al snapshot.data hasta que se resuelva  el 
      //cargarData o future:menuProvider.cargarData()
      //de lo contrario se genera error por que al inicio la lista es null.
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          children: _listaItems(snapshot.data!, context),
        );
      },
    );
  }

  List<Widget> _listaItems(List<dynamic> data, BuildContext context) {
    //creo una variable llamada opciones

    final List<Widget> opciones = [];

    data.forEach((opt) {
      final widgetTemp = ListTile(
        title: Text(opt['texto']),
        leading: getIcon(opt['icon']),
        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.blue),
        onTap: () {

          //esta es una forma de navegar entre paginas 
          // final route = MaterialPageRoute(
          //   builder: (context) => AlertPage());
          // Navigator.push(context, route);

        //La siguiente es otra forma de navegar entre paginas dentro del dispositivo.

       Navigator.pushNamed(context, opt['ruta']);
       //el pushNamed o el name de la rutas debe de estar definido en las
       //rutas de mi MaterialApp
        },
      );

      opciones..add(widgetTemp)
              ..add(Divider());
    });
    return opciones;
  }
}
