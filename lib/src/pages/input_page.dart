import 'package:flutter/material.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  String _nombre = '';
  String _email = '';
  String _fecha = '';
  String _opcionSeleccionada = 'Desarrollo en Flutter';
  List<String> _valores = [
    'Desarrollo en Flutter',
    'Ingeniero',
    'Desarrollador Web',
    'Moviles',
    'Bases datos',
    'Desarrollo Web',
    'MySql',
    'Android Kotlin',
    'Android Java'
  ];

//Este elemento o esta propiedad  _inputFieldDateController me permitira
//manejar una relacion con la caja de texto de fecha para poder cambiar sus
//propiedades de forma dinamica en otros lugares, asignarle valor.
//y a este elemento  necesito decirle que debera estar pendiente del control
//de la caja , y de ello se ocupara el metodo _crearFecha.
  TextEditingController _inputFieldDateController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inputs de texto'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: <Widget>[
          _crearInput(),
          Divider(),
          _crearEmail(),
          Divider(),
          _crearPersona(),
          Divider(),
          _crearPassword(),
          Divider(),
          _crearFecha(),
          Divider(),
          _crearDropdown(),
          //_crear
          // _crearPersona(),
        ],
      ),
    );
  }

  Widget _crearInput() {
    return TextField(
      //autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        //con el OutLineInputBorder se genera una caja de texto delineada
        border: OutlineInputBorder(
          //Con el borderRadius le doy forma redondeada a la caja de texto
          //generada anteriormente
          borderRadius: BorderRadius.circular(20.0),
        ),
        //counter: Text('Letras ${ _nombre.length }'),
        counter: Text('Letras ${_nombre.length}'),
        labelText: 'Nombre',
        hintText: 'Nombre de la persona',
        helperText: 'solo es el nombre',
        //icono que aparecera dentro del input
        suffixIcon: Icon(Icons.accessibility),
        icon: Icon(Icons.account_circle),
      ),
      //Con el onChange capturo el valor que este en la caja de texto
      //y se lo asigno a la variable  _nombre
      onChanged: (valor) {
        setState(() {
          _nombre = valor;
          print(_nombre);
        });
      },
    );
  }

  Widget _crearEmail() {
    return TextField(
        //autofocus: true,
        //textCapitalization: TextCapitalization.sentences,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          //con el OutLineInputBorder se genera una caja de texto delineada
          border: OutlineInputBorder(
              //Con el borderRadius le doy forma redondeada a la caja de texto
              //generada anteriormente
              borderRadius: BorderRadius.circular(20.0)),
          //counter: Text('Letras ${ _nombre.length }'),
          //counter: Text('Letras ${_nombre.length}'),
          hintText: 'Email',
          labelText: 'Email',
          // helperText: 'solo es el nombre',
          //icono que aparecera dentro del input
          suffixIcon: Icon(Icons.alternate_email),
          icon: Icon(Icons.email),
        ),
        //Con el onChange capturo el valor que este en la caja de texto
        //y se lo asigno a la variable  _email
        onChanged: (valor) => setState(() {
              _email = valor;
            }));
  }

  Widget _crearPassword() {
    return TextField(
        //autofocus: true,
        //textCapitalization: TextCapitalization.sentences,
        obscureText: true,
        decoration: InputDecoration(
          //con el OutLineInputBorder se genera una caja de texto delineada
          border: OutlineInputBorder(
              //Con el borderRadius le doy forma redondeada a la caja de texto
              //generada anteriormente
              borderRadius: BorderRadius.circular(20.0)),
          //counter: Text('Letras ${ _nombre.length }'),
          //counter: Text('Letras ${_nombre.length}'),
          hintText: 'password',
          labelText: 'Password',
          // helperText: 'solo es el nombre',
          //icono que aparecera dentro del input
          suffixIcon: Icon(Icons.lock_open),
          icon: Icon(Icons.lock),
        ),
        //Con el onChange capturo el valor que este en la caja de texto
        //y se lo asigno a la variable  _nombre
        onChanged: (valor) => setState(() {
              //_email = valor;
            }));
  }

  Widget _crearFecha() {
    return TextField(
      enableInteractiveSelection: false,

      //este elemento controller, sera el que se encargue del control de la
      // caja de fecha, y asi ya tengo asignado el controlador
      controller: _inputFieldDateController,
      //El obscureText:true, es para que se oculten los caracteres
      //obscureText: true,
      decoration: InputDecoration(
        //con el OutLineInputBorder se genera una caja de texto delineada
        border: OutlineInputBorder(
            //Con el borderRadius le doy forma redondeada a la caja de texto
            //generada anteriormente
            borderRadius: BorderRadius.circular(20.0)),
        //counter: Text('Letras ${ _nombre.length }'),
        //counter: Text('Letras ${_nombre.length}'),
        hintText: 'Fecha de nacimiento',
        labelText: 'Fecha nacimiento',
        // helperText: 'solo es el nombre''
        //icono que aparecera dentro del input
        suffixIcon: Icon(Icons.perm_contact_calendar),
        icon: Icon(Icons.calendar_today),
      ),

      //El onTap(){} nos sirve para que  dispare un metodo,que  nos permite
      //lanzar el selector de la fecha.
      //Lo primero es que el foco no se muestre alli en la cajonera de
      //seleccion  y para ello utilizamos el FocusScope.of().requestFocust()
      onTap: () {
        //esta linea quita el foco de la caja de texto de la fecha
        FocusScope.of(context).requestFocus(new FocusNode());
        _selecDate(context);
      },
    );
  }

  //Metodo de selecion de fecha
  _selecDate(BuildContext context) async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(2010),
      lastDate: new DateTime(2025),
      locale: Locale('es', 'ES'),
    );

    if (picked != null) {
      setState(() {
        _fecha = picked.toString();
        _inputFieldDateController.text = _fecha;
      });
    }
  }

//Genero una lista de valores para llenar el combo
  List<DropdownMenuItem<String>> getOpcionesDropdown() {
    //Creo una nueva lista que retornara un
    //DropdownMenuItem <String>>
    List<DropdownMenuItem<String>> lista = [];

    _valores.forEach((valor) {
      //Aqui ya tengo la lista generada con la propiedad valores
      lista.add(DropdownMenuItem(
        child: Text(valor),
        value: valor,
      ));
    });
    return lista;
  }

//Metodo crear ComboBox
  Widget _crearDropdown() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: <Widget>[
          Icon(Icons.select_all),
          SizedBox(
            width: 30.0,
          ),
          Expanded(
            child: DropdownButton(
                isExpanded: true,
                value: _opcionSeleccionada,
                items: getOpcionesDropdown(),
                onChanged: (opt) {
                  //print(opt);
                  setState(() {
                    _opcionSeleccionada = opt as String;
                  });
                }),
          )
        ],
      ),
    );
  }

  Widget _crearPersona() {
    return ListTile(
      title: Text('Nombre: $_nombre '),
      subtitle: Text('Email: $_email'),
      trailing: Text(_opcionSeleccionada),
    );
  }
}
