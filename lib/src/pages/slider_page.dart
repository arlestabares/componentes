import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  double _valorDelSlider = 200.0;
  bool _bloquearCheck = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Slider'),
      ),
      body: Container(
        //separo el container Column de la parte superior.
        padding: EdgeInsets.only(top: 80.0),
        child: Column(
          children: <Widget>[
            _crearSlider(),
            _checkBox2(),
            _crearSwitch(),
            Expanded(
                //child: _cargarImagenDeInternet()),
                child: _cargarImagenLocal()),
          ],
        ),
      ),
    );
  }

  Widget _crearSlider() {
    return Slider(
      //Le damos Color al Slider con el activeColor
      activeColor: Colors.indigoAccent,
      label: 'Corona Virus ',
      //divisions: 20,
      //Justo aqui el _valorDelSlider tiene un valor fijo, sera dinamico dentro
      //del metodo setState(){}
      value: _valorDelSlider,
      min: 10.0,
      max: 400.0,
      //El metodo onChange que va recibir el valor que tenga el slider
      //Pero aun aqui no se podra visualizar si se mueve, ya que para eso necesitamos
      //redibujar ese elemento con el metodo setState(){}, y podra visualizarse el
      //movimiento  del Slider.
      //Con la expresion ternaria hago la evaluacion,si _bloquearCheck
      //esta activo, regreso un null, en caso contrario llamo al metodo onChanged
      //con el parametro (valor) y ejecuta ese collback
      onChanged: (_bloquearCheck)
          ? null
          : (valor) {
              setState(() {
                _valorDelSlider = valor;
              });
              //print(valor);
            },
    );
  }

  Widget _cargarImagenLocal() {
    return Image(
        image: AssetImage('assets/coronavirus02.png'),
        width: _valorDelSlider,
        fit: BoxFit.contain);
  }

  Widget _checkBox2() {
    //La ventaja de utilizar un CheckboxListTile(), es que puedo hacer click en
    //cualquier parte del elemento y dispara el evento oncheck
    return CheckboxListTile(
        title: Text('Bloquear Slider..'),
        value: _bloquearCheck,
        onChanged: (valor) {
          setState(() {
            _bloquearCheck = valor!;
          });
        });
  }

  Widget _crearSwitch() {
    return SwitchListTile(
        title: Text('Bloquear Slider'),
        value: _bloquearCheck,
        onChanged: (valor) {
          setState(() {
            _bloquearCheck = valor;
          });
        });
  }
}

// Widget _checkBox() {
//   return Checkbox(
//       value: _bloquearCheck,
//       onChanged: (valor) {
//         setState(() {
//           _bloquearCheck = valor;
//         });
//       });
// }

// Widget _cargarImagenDeInternet() {
//   return Image(
//       image: NetworkImage(
//           'https://www.megaidea.net/wp-content/uploads/2020/03/coronavirus02.png'),
//       width: _valorDelSlider,
//       fit: BoxFit.contain);
// }
