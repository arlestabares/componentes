import 'dart:async';

import 'package:flutter/material.dart';

class ListaPage extends StatefulWidget {
  @override
  _ListaPageState createState() => _ListaPageState();
}

class _ListaPageState extends State<ListaPage> {
  //objeto _scrollController que me permitira controlar un
  //Scroll de varias listas o una sola.el cual se podra inicializar
  //en el  return ListView.builder(){}.
  ScrollController _scrollController = new ScrollController();
  List<int> _listaNumeros =  [];
  int _ultimoItem = 0;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Listas'),
      ),
      body: Stack(
        children: <Widget>[
          _crearLista(),
          _crearLoading(),
        ],
      ),
    );
  }

  @override
  void initState() {
    //Este super hace referencia a la clase State y la inicializa,
    // y lo sobreescribimos con el metodo _agregar10().
    super.initState();
    _agregar10();

    //Agregamos un Listener al _scrollController, y
    //alli metemos una funcion collback

    _scrollController.addListener(() {
      //print('SCROLL!!!!!!!');

      //El scroll tiene muchos valores, uno de ellos es la Posicion actual
      //en pixeles, el otro seria  cual es el maximo de pixeles que tiene.
      //Ejemplo: Si la posicion actual al estar abajo del todo el scroll es de
      //1000 pixeles y el Scroll maximo es de 1000 pixeles,
      //quiere decir que estamos al final de la pagina.
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        // _agregar10();

        fetchData();
      }
    });
  }

  //Este metodo es llamado para desechar la lista de listener cada vez que entro
  //en la pantalla del scroll de fotos, y para evitar la acumulacion en pila del
  //_scrollCrontroller se hara con el metodo dispose(){}., este metodo se dispara
  //cuando la pagina es destruida o cuando voy hacia atras en la pila de paginas.
  @override
  void dispose() {
    super.dispose();

    _scrollController.dispose();
  }

  //En este metodo sucede toda la magia, y cuando tire la pantalla
  //hacia abajo quiero que me diga o me indique que esta haciendo un
  //refresh y dejar que me muestre los primeros 10 registros unicamente,
  //Debo envolver el ListView.builder en otro widget que tenga un scroll.
  Widget _crearLista() {
    return RefreshIndicator(

      //Le paso la referencia del metodo _obtenerPagina1, no lo estoy 
      //ejecutando.
      onRefresh: _obtenerPagina1,
      child: ListView.builder(
        //En el ListView tenemos La propiedad controller a la que
        //le asignamos el  _scrollController, el cual me permitira moverlo
        controller: _scrollController,
        itemCount: _listaNumeros.length,
        itemBuilder: (BuildContext context, int index) {
          final imagen = _listaNumeros[index];

          //utilizo un FadeInImage porque la imagen la tengo que cargar de un recurso externo
          //lo mejor  es usar este tipo de imagenes para tener un loadin'
          return FadeInImage(
            image: NetworkImage('https://picsum.photos/500/300/?image=$imagen'),
            placeholder: AssetImage('assets/jar-loading.gif'),
          );
        },
      ),
    );
  }

  Future<void> _obtenerPagina1() {
    final duration = new Duration(seconds: 1);
    //Cuando pase ese segundo, quiero purgar la lista de numeros borrando
    //todo su contenido, sumo en 1 el ultimoItem, porque quiero nuevas imagenes
    //quiero las siguientes , luego llamar al _agregar10 para que  cargue 10 registros
    //nuevos.
    
    new Timer(duration, () {
      _listaNumeros.clear();
      _ultimoItem++;
      _agregar10();
    });
    return Future.delayed(duration);
  }

  void _agregar10() {
    for (var i = 0; i < 10; i++) {
      _ultimoItem++;
      _listaNumeros.add(_ultimoItem);
    }
    setState(() {});
  }

  //Con este metodo haremos una demora local mientras se
  //carga la informacion.
  Future<Timer> fetchData() async {
    //Este _isLoading cambiara cada que sea invocado este metodo
    //fetchData(), en el _scrollController.addListener(()) que contiene
    // el if(), donde  cada vez que se cumpla la condicion sera invocado
    _isLoading = true;

    //Con este metodo le digo a flutter que se redibuje porque
    // una propiedad cambio
    setState(() {});

    //Este metodo exige dos propiedades , una.... la duracion y otra el callback.
    //la duracion es el tiempo que se necesita para que se resuelva la
    //peticion para cargar las fotos, el segundo parametro sera un metodo
    //llamado respuestaHTTP(),
    //Lo que se desea es que cuando pasen los 2 segundos se dispare el metodo
    // respuestaHTTP()
    final duration = new Duration(seconds: 1);
    //el parametro respuestaHTTP lo pongo sin corchetes (), ya que no  necesito
    //que se ejecute, solo estoy pasando la referencia.
    return  Timer(duration, respuestaHTTP);
  }

  void respuestaHTTP() {
    //_isLoading = false , quiere decir que si ya pasaron los 2 segundos
    //es porque ya termine de cargar, por consiguiente lo que se debe hacer
    //es llamar al metodo  setState (){}, pero ese metodo ya esta en el metodo
    //_agregar10(){}, asi que llamo  a dicho metodo.

    _isLoading = false;

    //Cuando obtenemos la respuesta del respuestaHTTP(),yo puedo mover el
    //Scroll porque ya se que hay mas registros cargados, y para eso utilizo
    //el .animateTo(), que me pide la posicion en la cual yo quiero
    _scrollController.animateTo(
      _scrollController.position.pixels + 100,
      curve: Curves.fastOutSlowIn,
      duration: Duration(milliseconds: 250),
    );
    _agregar10();
  }

  Widget _crearLoading() {
    if (_isLoading) {
      //aqui retorna un circulo que gira en notificacion que esta cargando
      //la informacion, Utilizo un Column para que la notificacion
      //del CircularProgressIndicator() se muestre encima de el Scroll de fotos.
      //osea encima del contenedor de fotos
      return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[CircularProgressIndicator()],
          ),
          //Con el SizeBox() le doy separacion al final de la columna
          //ya que estoy dentro de un Column().
          SizedBox(height: 15.0)
        ],
      );
    } else {
      //Si no esta cargando debo retornar algo ya que es un widget
      //asi que retorno un Container vacio.
      return Container();
    }
  }
}
