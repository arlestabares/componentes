import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  final opciones = [
    'Cero',
    'Uno',
    'Dos',
    'Tres',
    'Cuatro',
    'Cinco',
    'Seis',
    'Siete',
    'Ocho',
    'Nueve',
    'Diez',
    'Once',
    'Doce',
    'Trece'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes APP'),
        centerTitle: true,
      ),
      // body: ListView(
      //children: _crearItems()
      //),
      body: ListView(children: _crearItemsFormaCorta()),
    );
  }

// /*
//  * Metodo para regresar una lista de forma larga
//  */
//   List<Widget> _crearItems() {
//     List<Widget> lista = new List<Widget>();
//     for (var opt in opciones) {
//       final tempWidget = ListTile(
//         title: Text(opt),
//       );
//       lista
//         ..add(tempWidget)
//         // lista..add(Divider());
//         ..add(Divider());
//     }
//     return lista;
//   }

/*
 * Metodo para retornar una lista de otra manera mostrando otra version.
 * de como anexar la lista  y agregando un par de widgets mas, uno al inicio del parrafo 
 * y otro al final del mismo
 *.
 */
  List<Widget> _crearItemsFormaCorta() {
    //Al map le llega una funcion como referencia  por ello retorna algo
    // var widgets = opciones.map((item) {
    //   return ListTile(
    //     title: Text(item + '!'),
    //   );
    // }).toList();

    //este return es igual que el anterior, solo que es mas
    //corto y no necesito crear una variable widgets

    // lo que estoy haciendo aqui es, regresando una nueva lista
    //cuyos items  han sido transformados pasandolos por el metodo del map
    // Ahora necesitamos poner el Divider(),para ello copiare el return y mencionare los cambios.

    // return opciones.map((item) {
    //   return ListTile(
    //     title: Text(item + '!'),
    //   );
    // }).toList();

//para poner el Divider ponemos el cursor sobre el ListTile y pinchamos
//command mas punto y seleccionamos la opcion Wrap with Column,
//esto automaticamente transformara el metodo  por un Column como el siguiente.
    //inmediatamente despues del listTile  puedo poner el Divider(), y los espacios
    //se veran reflejados

// Tambien existen mas propiedades para anexar
//como el subtitle justo debajo del title
//subtitle: Text('Cualquier cosa aqui')
    return opciones.map((item) {
      //el parametro item del map puede ser cualquier nombre.
      return Column(
        children: <Widget>[
          ListTile(
            title: Text(item + '!'),
            subtitle: Text('Esto es un subtitle'),
            //Agrega un widget al inicio de cada item de  la lista
            leading: Icon(Icons.account_balance_wallet),
            //agrega un widget al final de cada item de la lista
            trailing: Icon(Icons.keyboard_arrow_right),
            //Con la sola definicion de la funcion onTap:(){}, al hacer un click en  un item de la lista
            //reacciona a dicho evento
            onTap: () {},
          ),
        ],
      );
      //el .toList() convierte los elementos a una lista
    }).toList();
  }
}
